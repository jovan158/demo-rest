package com.demorest.projekat.demorest.Services;

import com.demorest.projekat.demorest.Entities.Student;

import java.util.List;
import java.util.Optional;

public interface StudentService {
    List<Student> getAllStudent();
    Optional<Student> getStudentById(Long id);
    Student saveStudent(Student student);
    void removeStudent(Student student);
}
